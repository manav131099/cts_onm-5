source('C:/Users/talki/Desktop/cec intern/codes/714/714 SMP10 Data extract.R')
rm(list=ls(all =TRUE))
library(ggplot2)
result <- read.table("C:/Users/talki/Desktop/cec intern/results/714/[714]_smp10_summary.txt",header = T)
result <- result[result[,2] >= 99,]

model <- lm(result[,3]~result[,4])
rsq <- format(as.numeric(summary(model)$r.squared),digits=3,nsmall = 3)
graph <- ggplot(result, aes(x=measure,y=model,group=Month))
p1 <- graph + geom_point(aes(shape = Month,colour = Month))
p1 <- p1 + scale_shape_manual(values=seq(0,length(unique(result$Month))-1))
p1 <- p1 +  theme_bw()
p1 <- p1 + geom_abline(intercept = 0, slope = 1)
p1 <- p1 + expand_limits(x=0,y=0)
p1 <- p1 + coord_cartesian(ylim=c(0,8),xlim=c(0,8))
p1 <- p1 + annotate("text",label = paste0("R-sq error:",rsq),size=4, x = 4, y= 1)
p1 <- p1 + ggtitle(paste("Satellite vs. 714 Ground Irradiation - Phnom Penh, Cambodia"))
p1 <- p1 + theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.8,hjust = 0.5))
p1 <- p1 + xlab(expression(paste("Ground Global Horizontal Irradiation [",kwh/m^2,", daily]")))
p1 <- p1 + ylab(expression(paste("Satellite Global Horizontal Irradiation [",kwh/m^2,", daily]")))

p1

ggsave(p1, filename = "C:/Users/talki/Desktop/cec intern/results/714/714 vs sat (PHNO).pdf", width = 7.92, height = 5)

source('C:/Users/talki/Desktop/cec intern/codes/714/714 monthly SMP10 vs PHNO sate graph.R')


